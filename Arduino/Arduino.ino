/*
 HC-SR04 Ping distance sensor:
 VCC to arduino 5v 
 GND to arduino GND
 Echo to Arduino pin 25 
 Trig to Arduino pin 22
 
 This sketch originates from Virtualmix: http://goo.gl/kJ8Gl
 Has been modified by Winkle ink here: http://winkleink.blogspot.com.au/2012/05/arduino-hc-sr04-ultrasonic-distance.html
 And modified further by ScottC here: http://arduinobasics.blogspot.com.au/2012/11/arduinobasics-hc-sr04-ultrasonic-sensor.html
 on 10 Nov 2012.

Modified by M.Angels Moncusi (DEIM-URV), to attach the sensor to the oficial Robot arduino on dec-2014

Javier Tris Enguita
Jon Ander Goicoechea Pojan
Francisco Aguaron Casas
 */ 


#define SLAVE_ADDRESS 0x04
#define echoPin 25  // Echo Pin
#define trigPin 22  // Trigger Pin
#define servoPin 20


#include <ArduinoRobot.h>
#include <Servo.h>

Servo myservo;



int minimumRange = 8;    // Minimum range needed
long distance; // Duration used to calculate distance
int state;
int dis;
int i = 0;
int lin;
void setup() {
   Wire.begin(SLAVE_ADDRESS);
   Robot.begin(); 
   Serial.begin (9600);
   Wire.onReceive(receiveData);
   Wire.onRequest(sendData); 
   pinMode(trigPin, OUTPUT);
   pinMode(echoPin, INPUT);
   myservo.attach(servoPin);
   myservo.write(90);// 120 rectos
}

void loop() {
  dis = getDistance();
  if(checkWhite())
    if(state!=4)
      lin=1;
  delay(50);
}


/*
  Method to send data to arduino depending on the last request received
  
*/
void sendData(){
  int c;
  switch(state){
    case 5:
      c=lin;
      lin = 0;
    break;
    case 7:
      c = dis; 
  break;
  }
  Wire.write(c);
}

/*
  Method for receiving data from the Raspberry perfoms
  actions or changes state for the responding requests.
*/
void receiveData(int byteCount){
  char number;
  while(Wire.available()) {
    number = Wire.read();
    Wire.flush();
    switch(number){
      case 0:
        stopCar(); 
      break;
      case 1:
      
        goForward();
      break;
      case 2:
        turnLeft();
      break;
      case 3:
        turnRight();
      break;
      case 4:
        goBackwards();
      break;
      case 5:
       //Llegir linia
        state=5;
      break;
      case 7:
         //Llegir distancia   
        state=7;
      break;     
    }  
  }
}


int getDistance(){
 long duration, distance; // Duration used to calculate distance
 
 digitalWrite(trigPin, LOW); 
 delayMicroseconds(2); 

 digitalWrite(trigPin, HIGH);
 delayMicroseconds(10); 
 
 digitalWrite(trigPin, LOW);
 duration = pulseIn(echoPin, HIGH);
 
 //Calculate the distance (in cm) based on the speed of sound.
 distance = duration/58.2;
 
 return distance;
}

void stopCar(){
  Robot.motorsStop();   
}
void turnLeft(){
   Robot.motorsWrite(-255/2, 255/2); // turn left
}
void turnRight(){
   Robot.motorsWrite(255/2, -255/2); // turn left
}
void goForward(){
   Robot.motorsWrite(255/2, 255/2);
}
void goBackwards(){
 Robot.motorsWrite(-255/2, -255/2);
}


boolean checkWhite(){
 Robot.updateIR();

  // iterate the array and print the data to the Serial port
  for(int i=0; i<5; i++){
    
    //Serial.print(Robot.IRarray[i]);
    if( Robot.IRarray[i]>=800){
     

      return true;  
  }
  }
  return false;
}
