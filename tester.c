#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <semaphore.h>

// The PiDistance board i2c address
#define ADDRESS 0x04

// The I2C bus: This is for V2 pi's. For V1 Model B you need i2c-0
static const char *devName = "/dev/i2c-1";

pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
sem_t sem_lines;
sem_t sem_distance;
struct sched_param sp;
pthread_attr_t attr;

pthread_t normal_stop;
pthread_t line_stop;
pthread_t check_distance;
pthread_t check_lines;

unsigned char cmd[16];

int file;

int receiveData(){
  char buf[1];
  int distance = 0;
  if (read(file, buf, 1) == 1)
      distance = (int)buf[0];
  return distance;
}

//TODO: sendData
void sendData(){
 write(file, cmd, 1);
 usleep(10000);
}


void * normalStop(id){
  char direction;
  do{
    sem_wait(&sem_distance);
    pthread_mutex_lock(&m);
    cmd[0]=6;
    sendData(); //STOP
    usleep(500);
    cmd[0]=6;
    sendData();
    direction = receiveData();
    sendData(direction);
	if(direction==4){
		usleep(100);
                cmd[0]=0;
		sendData();
                cmd[0]=6;
		sendData();
		direction = receiveData();
                 cmd[0]= direction;
		sendData();
	}
    printf("normalStop\n");
    pthread_mutex_unlock(&m);
  }while(1);
}

void * lineStop(id){
  char direction;
  do{
    sem_wait(&sem_lines);
    pthread_mutex_lock(&m);
    printf("lineStop\n");
    cmd[0]=0;
    sendData();
    usleep(50);
    cmd[0]=4;
    sendData();
    usleep(100);
    cmd[0]= 0;
    sendData();
    usleep(100);
    cmd[0]= 6;
    sendData();
    direction = receiveData();
    if(direction==4){
      cmd[0]= direction;
      sendData();
      usleep(100);
      cmd[0]=0;
      sendData();
      usleep(100);
      cmd[0]=6;
      sendData();
      direction = receiveData();
    }
    sendData(direction);
    pthread_mutex_unlock(&m);
  }while(1);
}

void * checkDistance(id){
/*
  int distance;
  char c;
  do{
    pthread_mutex_lock(&m);
    printf("checkDistance");
    //TODO: READ DISTANCE
    cmd[0]= 7;
    sendData();
    distance = receiveData();
    if(distance<16){
            printf("Inside Distance");
        sem_post(&sem_distance);
	}
    pthread_mutex_unlock(&m);

    usleep(50);
  }while(1);*/
}

void * checkLines(id){
  /*int i = 0;
  int c;
  printf("ESTOY AQUIII");
  do{
    pthread_mutex_lock(&m);

    cmd[0]=5;
    sendData();
 printf("checkLines %d\n",i%25);
    c = receiveData();
    if(c){
            printf("INSIDE");
	sem_post(&sem_lines);
    }
     printf("Blank %d\n",c);

    pthread_mutex_unlock(&m);
    usleep(50);
    i++;
  }while(1);*/
}





int main(int argc, char** argv) {
  int file,arg,val,distancia,policy;
  unsigned char cmd[16];
  char buf[1];

  pthread_mutex_init(&m,NULL);
  sem_init(&sem_distance, 0,0);
  sem_init(&sem_lines, 0,0);


  printf("I2C: Connecting\n");
  if ((file = open(devName, O_RDWR)) < 0) {
    fprintf(stderr, "I2C: Failed to access %d\n", devName);
    exit(1);
  }

  printf("I2C: acquiring buss to 0x%x\n", ADDRESS);
  if (ioctl(file, I2C_SLAVE, ADDRESS) < 0) {
    fprintf(stderr, "I2C: Failed to acquire bus access/talk to slave 0x%x\n", ADDRESS);
    exit(1);
  }

  cmd[0]=0;
  if (write(file, cmd, 1) == 1) {		//demanem distancia

      usleep(20000);
      printf("HOLA");
      }

  /*for (arg = 1; arg < argc; arg++) {
    if (0 == sscanf(argv[arg], "%d", &val)) {
      fprintf(stderr, "Invalid parameter %d \"%s\"\n", arg, argv[arg]);
      exit(1);
    }
    */


/*
    printf("Sending %d\n", val);
    cmd[0]=val;
    if (write(file, cmd, 1) == 1) {
      // As we are not talking to direct hardware but a microcontroller we
      // need to wait a short while so that it can respond.
      // 1ms seems to be enough but it depends on what workload it has
      usleep(10000);

      if (read(file, buf, 1) == 1) {
      distancia = (int)buf[0];
      printf("Received %d\n", distancia);
      }
    }

    // Now wait else you could crash the arduino by sending requests too fast
    usleep(10000);
  }
*/
  close(file);

  return (EXIT_SUCCESS);
}

